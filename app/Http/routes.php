<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');
Route::get('/', function(){
	$jscript=['web'];
	return view('welcome',array('jscript' =>$jscript));
});
use App\User;
Route::any('imail', function(){
	$dataa=\Input::all();
		/*
		$data['name']='edson';
		$data['lastname']='asdasd';
		$data['email']='eaduviri@geekadvice.pe';
		$data['message']='asdasd';
		*/


	try {

		$data['name']=$dataa['name'];
		$data['lastname']=$dataa['lastname'];
		$data['email']=$dataa['email'];
		$data['message']=$dataa['mensaje'];

		$user=new User();

		$validatoruser = $user->isValid($data);
		if ($validatoruser->passes()) {
			//return 'ok';
			$data['mensaje']=$data['message'];
			/*
			\Mail::send('emails.mail',$data, function($message)
			{
				$message->subject('Contact us');
			    $message->from('info@amanoyarns.com', 'Amano contact us');
			    $message->to('eaduviri@geekadvice.pe')->cc('eaduviri@geekadvice.pe');
			});
			*/

            \Mail::send('emails.mail',$data, function($message)
			{
				$message->subject('Contact us');
			    $message->from('info@amanoyarns.com', 'Amano contact us');
			    $message->to('fcruz@incatops.com')->cc('fcruz@incatops.com');
			});

			\Mail::send('emails.mail',$data, function($message)
			{
				$message->subject('Contact us');
			    $message->from('info@amanoyarns.com', 'Amano contact us');
			    $message->to('marketing@incatops.com')->cc('marketing@incatops.com');
			});
			\Mail::send('emails.correo',$data, function($message) use ($data)
			{
				$message->subject('Contact us');
			    $message->from($data['email'], 'Amano contact us');
			    $message->to('info@amanoyarns.com')->cc('info@amanoyarns.com');
			});
			\Mail::send('emails.mail',$data, function($message)
			{
				$message->subject('Contact us');
			    $message->from('info@amanoyarns.com', 'Amano contact us');
			    $message->to('agamazapata@gmail.com')->cc('agamazapata@gmail.com');
			});
			return 'ok';
		}
		else{
			 if ($validatoruser->fails()){
            	$asd=$validatoruser->messages()->toArray();
            	return $asd;
        	}
		}

	} catch (Exception $e) {
		return 'no';

	}
		return 'no';
});
Route::get('story', function(){
	$jscript=['store'];
	return view('story',array('jscript' =>$jscript));
});
Route::get('our-yarns',function(){
	$jscript=['jquery.bxslider.min','yarns'];
	return view('ouryarns',array('jscript' =>$jscript));
});
Route::get('our-patterns', function(){
	$jscript=['jquery.bxslider.min','patterns'];
	return view('ourpatterns',array('jscript' =>$jscript));
});
Route::get('contact-us', function(){
	$jscript=['jquery.validate.min','contact'];
	return view('contactus',array('jscript' =>$jscript));
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
