@extends('web')

@section('title') Our Patterns @endsection



@section('content')
    <?php
    $iurl = asset('/');
    $urljs = $iurl . 'js/';
    $urlimg = $iurl . 'img/';
    $urlpdf = $iurl . 'pdf/';
    ?>

    <div id="contslider">
        <div id="loading" class="loading"><div><img src="<?php echo $urlimg; ?>loading.gif"></div></div>
        <div id="contbigimg"><div id="velo"></div><div id="imgbig"><a style="cursor:pointer;" id="closebig">x</a><img id="imggbig" src=""></div></div>
        <div id="bxslider">


            <!--/////////////////patterns////////////////-->
            <!--/////////////////patterns////////////////-->
            <!--/////////////////patterns////////////////-->
            <div id="contpatterns">
                <div id="patterns">
                    <div id="patternsslider">
                        <!--////////////////////////APU////////////////////////-->
                        <div>
                            <div id="indexpatterns0" class="indexpatterns">
                                <div><a data-slide-index="0" style="background-color:#F2DECB;"></a></div>
                                <div><a data-slide-index="1" style="background-color:#AA7A49;"></a></div>
                            </div>
                            <div class="patters">
                                <div id="patternsslider0" class="sliderpatterns">
                                    <!--////////////////////////apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">laura cowl</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Apu', 'click', 'laura cowl');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">

                                                <div class="pattern_buy">
                                                    <div class="pattern-collection">
                                                        <b>Pattern Corrections</b><br/>
                                                        Inc/dec Rnd 3 should read:<br/>
                                                        *K1, M1p, (k1, p1d)) 11 times, sl2tog-k1-psso, (p1d, k1) 11 times, M1p; repeat from * 3 times.
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293527" > buy now </a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293527); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        2 Balls AMANO APU (25grs).<br>
                                                        in color # 1000 Pure White<br>
                                                        16" Circular Knitting needle size 4(3.50 mm)<br>
                                                        Stitch marker
                                                    </div>
                                                </div>
                                                <div id="indexalbun00" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun00" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--////////////////////////end apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">maria cowl</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Apu', 'click', 'maria cowl');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293529">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293529); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        2 Balls AMANO APU (25 grs),<br>
                                                        #1000 Pure Whitebr <br>
                                                        16â€ Length circular knitting needle, size 6 (4 mm) OR SIZE TO OBTAIN GAUGE
                                                        <br>
                                                        Stitch marker
                                                    </div>
                                                </div>
                                                <div id="indexalbun10" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun10" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <!--////////////////////////END APU////////////////////////-->

                        <!--////////////////////////MAYU////////////////////////-->
                        <div>
                            <div id="indexpatterns1" class="indexpatterns">
                                <div><a data-slide-index="0" style="background-color:#F2DECB;"></a></div>
                                <div><a data-slide-index="1" style="background-color:#AA7A49;"></a></div>
                                <div><a data-slide-index="2" style="background-color:#66302B;"></a></div>
                                <div><a data-slide-index="3" style="background-color:#872E1F;"></a></div>
                                <div><a data-slide-index="4" style="background-color:#D86F34;"></a></div>
                            </div>
                            <div class="patters">


                                <div id="patternsslider1" class="sliderpatterns">
                                    <!--////////////////////////apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">rosina mittens</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Mayu', 'click', 'rosina mittens');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/291252">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 291252); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        2 Hanks AMANO MAYU (50 grs),<br> #2003 Earth Brown
                                                        Double pointed knitting needles(dpn),<br> size 5 (3.75 mm) OR SIZE TO OBTAIN GAUGE Double pointed knitting needles (dpn), size 6 (4.00 mm) OR SIZE TO OBTAIN GAUGE Stitch markers
                                                        Waste yarn or sm stitch holders
                                                    </div>
                                                </div>
                                                <div id="indexalbun01" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun01" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--////////////////////////end apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">elsa vest</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Mayu', 'click', 'elsa vest');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293532">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293532); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        9(10-10-11-11-12) Hanks AMANO MAYU (50 grs), <br>
                                                        #2000 Frost White Straight knitting needles, <br>
                                                        size 5 (3.75 mm) OR SIZE TO OBTAIN GAUGE Straight knitting needles,<br>
                                                        size 6 (4.00 mm) OR SIZE TO OBTAIN GAUGE Cable needle (cn) Stitch markers
                                                    </div>
                                                </div>
                                                <div id="indexalbun11" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun11" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">paulina cowl</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Mayu', 'click', 'paulina cowl');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293534">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293534); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        2 Hanks AMANO MAYU (50 grs), #2004 Yellow Grass <br>
                                                        Straight knitting needle(s), size 6 (4 mm) OR SIZE TO OBTAIN GAUGE Straight knitting needle(s),<br>
                                                        size 7 (4.50 mm) OR SIZE TO OBTAIN GAUGE Small amount of waste yarn<br>
                                                        Stitch markers
                                                    </div>
                                                </div>
                                                <div id="indexalbun21" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun21" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">juanita cowl</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Mayu', 'click', 'juanita cowl');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293536">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293536); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        3 Hanks AMANO MAYU (50g), #2010 Stone Grey <br>
                                                        24â€ Circular knitting needle, size 6 (4 mm) OR SIZE TO OBTAIN GAUGE Cable needle (cn) <br>
                                                        Stitch markers<br>
                                                        Tapestry needle
                                                    </div>
                                                </div>
                                                <div id="indexalbun31" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun31" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">antonia cardigan</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Mayu', 'click', 'antonia cardigan');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293538">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293538); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        12(12-13-13-14-14) Hanks AMANO MAYU (50 grs), #2009 Mistery Lake<br>
                                                        24â€ Length circular knitting needles, size 7 (4.50 mm) OR SIZE TO OBTAIN GAUGE<br>
                                                        24â€ Length circular knitting needles, size 8 (5.00 mm) Double pointed needles (dpn), size 7 (4.50 mm) Stitch markers, <br>
                                                        Stitch holders
                                                    </div>
                                                </div>
                                                <div id="indexalbun41" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun41" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--////////////////////////END MAYU////////////////////////-->
                        <!--////////////////////////END MAYU////////////////////////-->

                        <!--////////////////////////MAYU////////////////////////-->
                        <div>
                            <div id="indexpatterns2" class="indexpatterns">
                                <div><a data-slide-index="0" style="background-color:#F2DECB;"></a></div>
                                <div><a data-slide-index="1" style="background-color:#AA7A49;"></a></div>
                                <div><a data-slide-index="2" style="background-color:#66302B;"></a></div>
                                <div><a data-slide-index="3" style="background-color:#872E1F;"></a></div>
                                <div><a data-slide-index="4" style="background-color:#D86F34;"></a></div>
                            </div>
                            <div class="patters">
                                <div id="patternsslider2" class="sliderpatterns">
                                    <!--////////////////////////apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">gloria hat</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puyu', 'click', 'gloria hat');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293539">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293539); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        2 Hanks AMANO PUYU (50 GRS), #3007 Silver <br>
                                                        16â€circular knitting needle, size 11 (8 mm)<br>
                                                        16â€ Circular knitting needle, size 13 (9 mm) OR SIZE TO OBTAIN GAUGE Double pointed knitting needles (dpns), size 13 (9 mm) Stitch marker Large pom pom maker<br>
                                                        Cable needle (cn)
                                                    </div>
                                                </div>
                                                <div id="indexalbun02" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun02" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--////////////////////////end apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">alejandrina shawl</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puyu', 'click', 'alejandrina shawl');
                                        </script>
                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293540">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293540); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        3 Hanks AMANO PUYU (50 grs), #3003 Caramel <br>
                                                        Straight knitting needles, size 13 (9.00 mm) OR SIZE TO OBTAIN GAUGE
                                                    </div>
                                                </div>
                                                <div id="indexalbun12" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun12" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">jacinta capelet</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puyu', 'click', 'jacinta capelet');
                                        </script>
                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293542">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293542); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        3 Hanks AMANO PUYU (50 grs),<br/>
                                                        in color #3001 Beige<br/>
                                                        24â€ Length circular knitting needle, size 13 (9.00 mm) OR SIZE TO<br/>
                                                        Stitch marker
                                                    </div>
                                                </div>
                                                <div id="indexalbun22" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun22" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">marcelina pullover</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puyu', 'click', 'marcelina pullover');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293543">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293543); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        7(8-8-9-9-10) Hanks AMANO PUYU (50 grs), <br>
                                                        #3001 Beige (MC) 3(3-3-4-4-4) Hanks AMANO PUYU (50 grs),<br>
                                                        #3000 Cloud (CC) Straight knitting needles,<br>
                                                        size 11 (8.00 mm) OR SIZE TO OBTAIN GAUGE 24â€ Length circular knitting needle, size 10 (6.00 mm)
                                                    </div>
                                                </div>
                                                <div id="indexalbun32" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun32" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">josefina pullover</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puyu', 'click', 'josefina pullover');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293544">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293544); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        10(11-12-13-14-15) Hanks AMANO PUYU (50 grs), #3008 Fog 24â€ Length circular knitting needle, size 11 (8.00 mm)
                                                        <br>
                                                        24â€ Length circular knitting needle, size 13 (9.00 mm)<br>
                                                        OR SIZE TO OBTAIN GAUGE<br>
                                                        Cable needle (cn), Stitch markers, Stitch holders
                                                    </div>
                                                </div>
                                                <div id="indexalbun42" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun42" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--////////////////////////MAYU////////////////////////-->
                        <!--////////////////////////MAYU////////////////////////-->
                        <div>
                            <div id="indexpatterns3" class="indexpatterns">
                                <div><a data-slide-index="0" style="background-color:#F2DECB;"></a></div>
                                <div><a data-slide-index="1" style="background-color:#AA7A49;"></a></div>
                                <div><a data-slide-index="2" style="background-color:#66302B;"></a></div>
                            </div>
                            <div class="patters">
                                <div id="patternsslider3" class="sliderpatterns">
                                    <!--////////////////////////apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">frida scarf</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puna', 'click', 'frida scarf');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div class="pattern-collection">
                                                        <b>Pattern Corrections</b><br/>
                                                        Row 28 should read:<br/>
                                                        K4, (k2tog, yo, k1) twice, yo, ssk, k1, yo, ssk, k2tog, yo, k6. Repeat rows 1 â€“ 28 for pattern.
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293545">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293545); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        3 Hanks AMANO PUNA (100 grs), #4005 Alpamayo <br>
                                                        Straight knitting needles, size 7 (4.50 mm) OR SIZE TO OBTAIN GAUGE Stitch markers

                                                    </div>
                                                </div>
                                                <div id="indexalbun03" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun03" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--////////////////////////end apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">dorita pullover</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puna', 'click', 'dorita pullover');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293546">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293546); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        8(8-8-9-9-9) Hanks AMANO PUNA (100 grs), #4000 Maras White <br>
                                                        24â€ Circular knitting needle, size 4 (3.50 mm) OR SIZE TO OBTAIN GAUGE Straight knitting needles, <br>
                                                        size 5 (3.75 mm) OR SIZE TO OBTAIN GAUGE Stitch markers <br>
                                                        Stitch holders
                                                    </div>
                                                </div>
                                                <div id="indexalbun13" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun13" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">delia cowl</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puna', 'click', 'delia cowl');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293548">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293548); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        1 Hank AMANO PUNA (100g), #4006 Inca Train Straight knitting needles, <br>
                                                        size 6 (4 mm) Straight knitting needles, size 8 (5 mm) <br>
                                                        OR SIZE TO OBTAIN GAUGE <br>
                                                        Tapestry needle
                                                    </div>
                                                </div>
                                                <div id="indexalbun23" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun23" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">lupe cardigan</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Puna', 'click', 'lupe cardigan');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <!-- <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div> -->
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">

                                                        <a target="_blank" href="<?php echo $urlpdf; ?>lupe_cardigan.pdf">
                                                            download<br>
                                                            free<br>
                                                            pattern
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        6(7-7-7-8-8) Hanks AMANO PUNA (100 grs),  <br>
                                                        #4006 Inca Train  <br>
                                                        24â€ Length circular knitting needle, size 4 (3.50 mm)
                                                        OR SIZE TO OBTAIN GAUGE <br>
                                                        24â€ Length circular knitting needle, size 5 (3.75 mm) <br>
                                                        OR SIZE TO OBTAIN GAUGE <br>
                                                        Cable needle (cn) Stitch markers

                                                    </div>
                                                </div>
                                                <div id="indexalbun33" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun33" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!--////////////////////////MAYU////////////////////////-->
                        <!--////////////////////////MAYU////////////////////////-->
                        <div>
                            <div id="indexpatterns4" class="indexpatterns">
                                <div><a data-slide-index="0" style="background-color:#F2DECB;"></a></div>
                            </div>
                            <div class="patters">
                                <div id="patternsslider4" class="sliderpatterns">
                                    <!--////////////////////////apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">isabel shawl</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Warmi', 'click', 'isabel shawl');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293550">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293550); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        3 Hanks AMANO WARMI (100 grs), #6010 Chia <br>
                                                        29â€ circular knitting needle, size 9 (5.50 mm) OR SIZE TO OBTAIN GAUGE
                                                    </div>
                                                </div>
                                                <div id="indexalbun04" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun04" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--////////////////////////end apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">hilda hat</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Warmi', 'click', 'hilda hat');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293551">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293551); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        1 Hank AMANO WARMI (100 grs), <br>
                                                        #6009 Blue Berry 16â€ Length circular knitting needle, size 8 (5.00 mm) 16â€ Length circular knitting needle,<br>
                                                        size 9 (5.50 mm) Double pointed needles, size 9 (5.50 mm)<br>
                                                        Stitch markers
                                                    </div>
                                                </div>
                                                <div id="indexalbun14" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun14" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">anita wrap vest</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Warmi', 'click', 'anita wrap vest');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293554">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293554); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        9(9-9-10-10-11) Hanks AMANO WARMI (100 grs), <br>
                                                        #6009 Blue Berry Straight knitting needles, size 6 (4.00 mm) <br>
                                                        Straight knitting needles, size 7 (4.50 mm) OR SIZE TO OBTAIN GAUGE Double pointed needles (dpn), size 8 (5.00 mm) for belt <br>
                                                        Stitch markers
                                                    </div>
                                                </div>
                                                <div id="indexalbun24" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun24" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-center">dominga top</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Warmi', 'click', 'dominga top vest');
                                        </script>

                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293556">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293556); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        6(8-10) Hanks AMANO WARMI (100 grs), #6010 Chia <br>
                                                        24â€ Length circular knitting needle, size 7 (4.50 mm) OR SIZE TO OBTAIN GAUGE <br>
                                                        Cable needle (cn), Stitch markers, Stitch holders
                                                    </div>
                                                </div>
                                                <div id="indexalbun34" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun34" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--////////////////////////MAYU////////////////////////-->
                        <!--////////////////////////MAYU////////////////////////-->
                        <div>
                            <div id="indexpatterns5" class="indexpatterns">
                                <div><a data-slide-index="0" style="background-color:#F2DECB;"></a></div>
                                <div><a data-slide-index="1" style="background-color:#AA7A49;"></a></div>
                                <div><a data-slide-index="2" style="background-color:#66302B;"></a></div>
                                <div><a data-slide-index="3" style="background-color:#872E1F;"></a></div>
                            </div>
                            <div class="patters">
                                <div id="patternsslider5" class="sliderpatterns">
                                    <!--////////////////////////apu1////////////////////////-->
                                    <div>
                                        <h1 class="text-center">benita scarf</h1>
                                        <script>
                                            ga('send', 'event', 'OurPatterns-Ayni', 'click', 'benita scarf');
                                        </script>
                                        <div class="patternlevel">
                                            <b>skill level</b><img width="12"
                                                                   src="<?php echo $urlimg; ?>punto0.png"><img
                                                    width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"
                                                                                                           src="<?php echo $urlimg; ?>punto1.png">
                                            Intermediate <span><b>one size</b></span>
                                            <div class="pull-right showcar"> <a class="text-link-buy" href="#" onclick="R.cart.show(52560); return false;"><span class="glyphicon glyphicon-shopping-cart"></span><b> show cart</b></a></div>
                                        </div>
                                        <div class="contalbun">
                                            <div class="col-sm-6 colum-xs text-right">
                                                <div class="pattern_buy">
                                                    <div>
                                                    </div>
                                                    <div class="box_buy">
                                                        <a href="http://www.ravelry.com/purchase/amano-yarns/293549">buy now</a>
                                                        or<br>
                                                        <a href="#" onclick="R.cart.add(52560, 293549); return false;">add to cart</a>
                                                    </div>
                                                </div>
                                                <div class="contmaterial">
                                                    <div>
                                                        <b>materials</b><br>
                                                        2 Hanks AMANO AYNI (50 grs), #5007 Poppy Blue <br>
                                                        Straight knitting needles, size 6 (4.00 mm)<br>
                                                        Straight knitting needles, size 8 (5.00 mm) OR SIZE TO OBTAIN GAUGE
                                                    </div>
                                                </div>
                                                <div id="indexalbun05" class="indexalbun">
                                                    <!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

                                                </div>
                                            </div>
                                            <div class="col-sm-6 colum-xs">
                                                <div id="albun05" class="albun">
                                                    <!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--////////////////////////end apu1////////////////////////-->

                                </div>
                            </div>
                        </div>
                        <!--////////////////////////MAYU////////////////////////-->

                    </div>
                </div>

                <div class="indexgalery">
                    <div id="pattersindex">
                        <!--
                        <a id="aapu2" data-slide-index="0"></a>
                        <a id="amayu2" data-slide-index="1"></a>
                        <a id="apuyu2"  data-slide-index="2"></a>
                        <a id="apuna2"  data-slide-index="3"></a>
                        <a id="awarmi2"  data-slide-index="4"></a>
                        <a id="aayni2"  data-slide-index="5"></a>
                        -->
                    </div>
                </div>


            </div>
            <!--/////////////////end patterns////////////////-->

        </div>
    </div>
    <div>
    </div>
    <a id="acomodin"></a>
@endsection

