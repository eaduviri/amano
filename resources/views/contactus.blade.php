@extends('web')

@section('title') Contact @endsection
@section('content')
<?php
$iurl=asset('/');
$urljs=$iurl.'js/';
$urlimg=$iurl.'img/';
?>
	<div class="contmensaje"><div id="velo"></div><div class="mensaje"><div id="msgform"></div><div id="loading"><img width="50" src="<?php echo $urlimg; ?>loading.gif"></div><a href="#" id="closemen">x</a></div></div>
	<div id="contslider">
			<div id="bxslider">

		  		<div id="contcontact">
		  			<div id="logocontat">
		  				<img src="<?php echo $urlimg; ?>logoblack.png">
		  			</div>
		  			<div class="ttlcontact">
		  				contact us
		  			</div>
		  			<div id="contformcontact">
		  				<form id="formcontact">
	  						<div class="form-group">
						    <label >first name</label>
						    <div class="redalert" id="error1"></div>
						    <input type="text" class="form-control"  minlength="2" maxlength="30" id="name" required>
						  </div>
						  <div class="form-group">
						    <label >last name</label>
						    <div class="redalert" id="error2"></div>
						    <input type="text" class="form-control" id="lastname" minlength="2" maxlength="30" required>
						  </div>
						  <div class="form-group">
						    <label for="Email">Email address</label>
						    <div class="redalert" id="error3"></div>
						    <input type="email" class="form-control" id="email" placeholder="Email" minlength="2" maxlength="50" required>
						  </div>
						  <div class="form-group">
						    <label for="exampleInputFile">message</label>
						    <div class="redalert" id="error4"></div>
						    <textarea id="mensaje" rows="5" class="form-control" maxlength="360" required></textarea>
						  </div>

						  <button class="form-control" type="submit" class="btn btn-default">Send message</button>
						</form>
		  			</div>

		  		</div>

			</div>
	</div>
	<script type="text/javascript">
		var token='<?php echo csrf_token(); ?>';
	</script>
	@endsection