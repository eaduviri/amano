<?php
$iurl=asset('/');
$urljs=$iurl.'js/';
$urlimg=$iurl.'img/';
?>
<html>
	<head>
		<title>Amano</title>

		<?php
		 echo
		 '<link rel="stylesheet" type="text/css" href="'.$iurl.'/css/style.css">
		 <link rel="stylesheet" type="text/css" href="'.$iurl.'/css/app.css">
		 <script type="text/javascript"> var urlimg="'.$urlimg.'"; </script>
		 '
		 ; ?>
	</head>
	<body>

	<style type="text/css" media="screen">
		#edson,#edson2{
			display: none;
		}
		#edson{
			height: 200px;
			background-color: #000;
		}
		#edson2{
			height: 200px;
			background-color: #ccc;
		}
		#edsonindex{
			height: 50px;
			background-color: #f0f;
			color: #fff;
		}
		#contedson .active{
			display: block;
		}
		#yarnsslider{
			height: 100px;
		}
		.bx-viewport{
			height: 100% !important;
		}
	</style>
	<div id="contvideo">
		<video id="video" width="100%"  autoplay loop>
		  <source src="<?php echo $iurl; ?>video/video.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
	</div>
	<div id="contslider">
				<div id="bxslider">
				<!--/////////////////HOME////////////////-->
				<!--/////////////////HOME////////////////-->
				<!--/////////////////HOME////////////////-->
		  		<div>
		  			<div id="logo1"><img src="<?php echo $urlimg; ?>logo.png"></div>
		  		</div>
		  		<!--///////////////// STORY////////////////-->
		  		<!--///////////////// STORY////////////////-->
		  		<!--///////////////// STORY////////////////-->
		  		<div>
		  			<div id="logo2"><img src="<?php echo $urlimg; ?>logo2.png"></div>
		  			<div id="texto">
		  				<div class="1">FOR  THOUSANDS OF YEARS THE PEOPLE OF THE ANDES HAVE MADE OFFERINGS TO THE GODS</div>
						<div class="2">THAT THEY BELIEVE DWELL IN THE MOUNTAINS, THE EARTH THE RIVERS AND THE SKY.</div>
						<div class="3">WITH AMANO WE WANT TO BRING BACK THOSE MAGICAL BELIEFS TO OUR TIME AND THROUGH OUR HANDS,</div>
						<div class="4">WITH CREATIV TY, FEE THE NATURAL RESOURCES OF THE PERUVIAN ANDES OURSELVES.</div>
						<div class="5">AMANO IS LUXURY IN ITS PUREST EXPRESSION AND REPRESENTS GENEROSITY,</div>
						<div class="6">A GIFT FROM GOODS, A TASTE OF THE ANDES.</div>
		  			</div>
		  		</div>
		  		<!--///////////////// YARNS////////////////-->
		  		<!--///////////////// YARNS////////////////-->
		  		<!--///////////////// YARNS////////////////-->
		  		<div id="contyarns">
		  			<div id="yarns">
		  				<div id="yarnsslider" >
		  				<!--///////////////// APU YARNS////////////////-->
		  				<!--///////////////// APU YARNS////////////////-->
					  		<div>
		  						<!--/////////////////APU COLORS////////////////-->
		  						<div id="ttllogo1" class="ttllogo"></div>
					  			<div>
					  				<div class="contcolorslider">
					  					<div class="colorslider" id="colorslider0">

					  					</div>
						  			</div>

						  			<div id="indexcolors0" class="indexcolors"></div>
					  			</div>
					  			<h2>60% roytal alpaca · 20% cashmere · 20% silk</h2>
					  			<div class="subttlcolors">weight 50gr| approx. 109yd / 100m | needle size 5 US</div>
					  			<div class="conttxtcolors">
					  				Mayu in Quechua (the native languaje of the incas) means rivers. This unique Blend or royal alpaca cashmere and mulberry silk has an incomparable softness and fluid appearance all colors are dip dyed imitating a dyeing technique used by the incas. the inspiration for colors comes from the soft the landscapes of the peruvian andes mountains.
					  			</div>
					  			<!--/////////////////END APU COLORS////////////////-->
					  		</div>
					  		<!--/////////////////END APU YARNS////////////////-->
		  					<!--/////////////////END APU YARNS////////////////-->

					  		<!--///////////////// MAYU YARNS////////////////-->
					  		<!--///////////////// MAYU YARNS////////////////-->
		  					<div>
		  						<!--/////////////////APU COLORS////////////////-->
		  						<div id="ttllogo2" class="ttllogo"></div>
					  			<div>
					  				<div class="contcolorslider">
					  					<div class="colorslider" id="colorslider1">
					  					</div>
						  			</div>

						  			<div id="indexcolors1" class="indexcolors">
						  			</div>
					  			</div>
					  			<h2>60% roytal alpaca · 20% cashmere · 20% silk</h2>
					  			<div class="subttlcolors">weight 50gr| approx. 109yd / 100m | needle size 5 US</div>
					  			<div class="conttxtcolors">
					  				Mayu in Quechua (the native languaje of the incas) means rivers. This unique Blend or royal alpaca cashmere and mulberry silk has an incomparable softness and fluid appearance all colors are dip dyed imitating a dyeing technique used by the incas. the inspiration for colors comes from the soft the landscapes of the peruvian andes mountains.
					  			</div>
					  			<!--/////////////////END APU COLORS////////////////-->
					  		</div>
					  		<!--/////////////////END MAYU YARNS////////////////-->
					  		<!--/////////////////END MAYU YARNS////////////////-->

					  		<!--///////////////// PUYU YARNS////////////////-->
					  		<!--///////////////// PUYU YARNS////////////////-->
					  		<div>
					  			<!--/////////////////PUYU COLORS////////////////-->
					  			<div id="ttllogo3" class="ttllogo"></div>
					  			<div>
					  				<div class="contcolorslider">
					  					<div class="colorslider" id="colorslider2">
					  					</div>
						  			</div>

						  			<div id="indexcolors2" class="indexcolors">
						  			</div>
					  			</div>
					  			<h2>60% roytal alpaca · 20% cashmere · 20% silk</h2>
					  			<div class="subttlcolors">weight 50gr| approx. 109yd / 100m | needle size 5 US</div>
					  			<div class="conttxtcolors">
					  				Mayu in Quechua (the native languaje of the incas) means rivers. This unique Blend or royal alpaca cashmere and mulberry silk has an incomparable softness and fluid appearance all colors are dip dyed imitating a dyeing technique used by the incas. the inspiration for colors comes from the soft the landscapes of the peruvian andes mountains.
					  			</div>
					  			<!--/////////////////END PUYU COLORS////////////////-->
					  		</div>
					  		<!--/////////////////END PUYU YARNS////////////////-->
					  		<!--/////////////////END PUYU YARNS////////////////-->


					  		<!--///////////////// PUNA YARNS////////////////-->
					  		<!--///////////////// PUNA YARNS////////////////-->
					  		<div>
					  			<!--/////////////////PUNA COLORS////////////////-->
					  			<div id="ttllogo4" class="ttllogo"></div>
					  			<div>
					  				<div class="contcolorslider">
					  					<div class="colorslider" id="colorslider3">
					  					</div>
						  			</div>

						  			<div id="indexcolors3" class="indexcolors">
						  			</div>
					  			</div>
					  			<h2>60% roytal alpaca · 20% cashmere · 20% silk</h2>
					  			<div class="subttlcolors">weight 50gr| approx. 109yd / 100m | needle size 5 US</div>
					  			<div class="conttxtcolors">
					  				Mayu in Quechua (the native languaje of the incas) means rivers. This unique Blend or royal alpaca cashmere and mulberry silk has an incomparable softness and fluid appearance all colors are dip dyed imitating a dyeing technique used by the incas. the inspiration for colors comes from the soft the landscapes of the peruvian andes mountains.
					  			</div>
					  			<!--/////////////////END PUNA COLORS////////////////-->
					  		</div>
					  		<!--///////////////// END PUNA YARNS////////////////-->
					  		<!--///////////////// END PUNA YARNS////////////////-->

					  		<!--///////////////// WARMI YARNS////////////////-->
					  		<!--///////////////// WARMI YARNS////////////////-->
					  		<div>
					  		<!--/////////////////WARMI COLORS////////////////-->
					  			<div id="ttllogo5" class="ttllogo"></div>
					  			<div>
					  				<div class="contcolorslider">
					  					<div class="colorslider" id="colorslider4">
					  					</div>
						  			</div>

						  			<div id="indexcolors4" class="indexcolors">
						  			</div>
					  			</div>
					  			<h2>60% roytal alpaca · 20% cashmere · 20% silk</h2>
					  			<div class="subttlcolors">weight 50gr| approx. 109yd / 100m | needle size 5 US</div>
					  			<div class="conttxtcolors">
					  				Mayu in Quechua (the native languaje of the incas) means rivers. This unique Blend or royal alpaca cashmere and mulberry silk has an incomparable softness and fluid appearance all colors are dip dyed imitating a dyeing technique used by the incas. the inspiration for colors comes from the soft the landscapes of the peruvian andes mountains.
					  			</div>

					  			<!--/////////////////END WARMI COLORS////////////////-->
					  		</div>
					  		<!--///////////////// END WARMI YARNS////////////////-->
					  		<!--///////////////// END WARMI YARNS////////////////-->

					  		<!--///////////////// AYMI YARNS////////////////-->
					  		<!--///////////////// AYMI YARNS////////////////-->
					  		<div>
					  		<!--/////////////////AYMI COLORS////////////////-->
					  			<div id="ttllogo6" class="ttllogo"></div>
					  			<div>
					  				<div class="contcolorslider">
					  					<div class="colorslider" id="colorslider5">
					  					</div>
						  			</div>

						  			<div id="indexcolors5" class="indexcolors">
						  			</div>
					  			</div>
					  			<h2>60% roytal alpaca · 20% cashmere · 20% silk</h2>
					  			<div class="subttlcolors">weight 50gr| approx. 109yd / 100m | needle size 5 US</div>
					  			<div class="conttxtcolors">
					  				Mayu in Quechua (the native languaje of the incas) means rivers. This unique Blend or royal alpaca cashmere and mulberry silk has an incomparable softness and fluid appearance all colors are dip dyed imitating a dyeing technique used by the incas. the inspiration for colors comes from the soft the landscapes of the peruvian andes mountains.
					  			</div>
					  			<!--/////////////////END AYMI COLORS////////////////-->
					  		</div>
					  		<!--///////////////// END AYMI YARNS////////////////-->
					  		<!--///////////////// END AYMI YARNS////////////////-->
		  				</div>
		  			</div>
			  		<div class="indexgalery">
		  				<div id="yarnsindex">
			  				<!--<a id="aapu" data-slide-index="0"></a>
				  			<a id="amayu" data-slide-index="1"></a>
				  			<a id="apuyu"  data-slide-index="2"></a>
				  			<a id="apuna"  data-slide-index="3"></a>
				  			<a id="awarmi"  data-slide-index="4"></a>
				  			<a id="aayni"  data-slide-index="5"></a>-->
			  			</div>
		  			</div>
		  		</div>
		  		<!--/////////////////end yarnss////////////////-->
		  		<!--/////////////////end yarnss////////////////-->
		  		<!--/////////////////end yarnss////////////////-->


		  		<!--/////////////////patterns////////////////-->
		  		<!--/////////////////patterns////////////////-->
		  		<!--/////////////////patterns////////////////-->
		  		<div id="contpatterns">
		  		<div id="patterns">
		  				<div id="patternsslider">
		  					<!--////////////////////////APU////////////////////////-->
		  					<div>
		  						<div id="indexpatterns0" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a  data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider0" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy">
					  										buy this <br> pattern
					  									</div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun0" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun0" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun1" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun1" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun2" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun2" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun3" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun3" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun4" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun4" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>

					  		</div>
					  		<!--////////////////////////END APU////////////////////////-->

					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns1" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a  data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider1" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun01" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun01" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun11" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun11" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun21" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun21" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun31" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun31" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun41" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun41" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////END MAYU////////////////////////-->
					  		<!--////////////////////////END MAYU////////////////////////-->

					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns2" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a  data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider2" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun02" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun02" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun12" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun12" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun22" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun22" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun32" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun32" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun42" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun42" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns3" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a  data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider3" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun03" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun03" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun13" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun13" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun23" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun23" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun33" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun33" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun43" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun43" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns4" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a  data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider4" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun04" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun04" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun14" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun14" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun24" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun24" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun34" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun34" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun44" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun44" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns5" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a  data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider5" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun05" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun05" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun15" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun15" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun25" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun25" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun35" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun35" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<div class="box_buy"></div>
					  								</div>
					  								<div >
					  									<b>materials</b><br>
						  								2 Balls AMANO APU (25grs).<br>
						  								in color # 1000 Pure White<br>
						  								16'' Circular Knitting needle size 4(3.50 mm)<br>
						  								Stitch marker
					  								</div>
					  								<div id="indexalbun45" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun45" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////MAYU////////////////////////-->

		  				</div>
		  			</div>

		  			<div class="indexgalery">
		  				<div id="pattersindex">
		  					<!--
			  				<a id="aapu2" data-slide-index="0"></a>
				  			<a id="amayu2" data-slide-index="1"></a>
				  			<a id="apuyu2"  data-slide-index="2"></a>
				  			<a id="apuna2"  data-slide-index="3"></a>
				  			<a id="awarmi2"  data-slide-index="4"></a>
				  			<a id="aayni2"  data-slide-index="5"></a>
				  			-->
		  				</div>
		  			</div>


		  		</div>
		  		<!--/////////////////end patterns////////////////-->
		  		<div id="contcontact">
		  			<div id="logocontat">
		  				<img src="<?php echo $urlimg; ?>logoblack.png">
		  			</div>
		  			<div class="ttlcontact">
		  				contact us
		  			</div>
		  			<div id="formcontact">
		  				<form>
	  						<div class="form-group">
						    <label >first name</label>
						    <input type="text" class="form-control" id="name" >
						  </div>
						  <div class="form-group">
						    <label >last name</label>
						    <input type="text" class="form-control" id="lastname" >
						  </div>
						  <div class="form-group">
						    <label for="Email1">Email address</label>
						    <input type="email" class="form-control" id="Email1" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputFile">message</label>
						    <textarea rows="5" class="form-control"></textarea>
						  </div>

						  <button class="form-control" type="submit" class="btn btn-default">Send message</button>
						</form>
		  			</div>

		  		</div>

			</div>
		</div>
		<div>

		</div>

		<div id="bx-pager">
			<div><a id="alogo" data-slide-index="0" href=""><img width="155" src="<?php echo $urlimg; ?>logomenu.png"></a></div>
			<div><a id="astory" data-slide-index="1" href="">story</a></div>
			<div><a id="ayarns" data-slide-index="2" href="">our yarns</a></div>
			<div><a id="apatterns" data-slide-index="3" href="">our patterns</a></div>
			<div><a id="astore" href="#">store finder</a></div>
			<div><a data-slide-index="4" href="">contact us</a><span id="contsocial"><a id="instagram" href="#" class="social"><span class="icon icon-instagram"></span></a><a href="" class="social"><img width="16px" src="<?php echo $urlimg; ?>icor.png"></a><a href="#" class="social"><span class=" icon icon-facebook2"></span></a></span></div>
			<!--<span class="icon icon-twitter2"></span>-->
		</div>


		<?php
		echo '<script src="'.$urljs.'jquery-1.11.3.min.js" type="text/javascript"></script>
			<script src="'.$urljs.'jquery.bxslider.min.js" type="text/javascript"></script>
			<script src="'.$urljs.'home.js" type="text/javascript"></script>';
		?>
	</body>
</html>
