<?php
$iurl=asset('/');
$urljs=$iurl.'js/';
$urlimg=$iurl.'img/';
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

		<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

        <title>@yield('title')- Peruvian Alpaca Luxury yarn and patterns</title>


        <meta name="description" content="Luxury Yarn and patterns in Imperial, Royal, Baby Alpaca and Alpaca Blends for Hand knitting. Colors and qualities inspired in the Peruvian Andes.">

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php
		 echo
		 '<link rel="stylesheet" type="text/css" href="'.$iurl.'css/app.css">
		 <link rel="stylesheet" type="text/css" href="'.$iurl.'css/style.css">
		 <script type="text/javascript"> var urlimg="'.$urlimg.'"; </script>
		 <script src="http://js.ravelry.com/cart/2.0.js" type="text/javascript"></script>
		 <script>
			(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,"script","//www.google-analytics.com/analytics.js","ga");

			ga("create", "UA-63597544-1", "auto");
			ga("send", "pageview");

		</script>
		 '
		 ; ?>


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
	<body>
		<div class="header-menu">
			<div class="btn btn-menu" type="button" data-toggle="collapse" data-target="#collapseMenu"><span class="glyphicon glyphicon-align-justify"  ></span></div>
			<div class="body-menu collapse" id="collapseMenu">
				<div id="logomenu">
	  				<img src="<?php echo $urlimg; ?>logoblack.png">
	  			</div>
				<div><a id="alogo" class="<?php echo Request::is('/')?'active':''; ?>" data-slide-index="0" href="<?php echo URL::to('/'); ?>">home</a></div>
				<div><a id="astory" class="<?php echo Request::is('story')?'active':''; ?>" data-slide-index="1" href="<?php echo URL::to('story'); ?>">story</a></div>
				<div><a id="ayarns" class="<?php echo Request::is('our-yarns')?'active':''; ?>" data-slide-index="2" href="<?php echo URL::to('our-yarns'); ?>">our yarns</a></div>
				<div><a id="apatterns" class="<?php echo Request::is('our-patterns')?'active':''; ?>" data-slide-index="3" href="<?php echo URL::to('our-patterns'); ?>">our patterns</a></div>
				<div><a id="astore" target="_blank"  href="http://www.berroco.com/locator/amano.php">store finder</a></div>
				<div><a data-slide-index="4" href="<?php echo URL::to('contact-us'); ?>">contact us</a></div>
				<div><span id="contsocial"><a  target="_blank" id="instagram" href="https://instagram.com/amanoyarns/ " class="social"><span class="icon icon-instagram"></span></a><a target="_blank" href="http://www.ravelry.com/yarns/brands/amano-yarns" class="social"><img width="20px" src="<?php echo $urlimg; ?>icor.png"></a><a target="_blank" href="https://www.facebook.com/AmanoYarns" class="social"><span class=" icon icon-facebook2"></span></a></span></div>
			</div>
		</div>

		@yield('content')
		<div id="bx-pager">
			<div><a id="alogo" class="<?php echo Request::is('/')?'active':''; ?>" data-slide-index="0" href="<?php echo URL::to('/'); ?>"><img width="100" src="<?php echo $urlimg; ?>logomenu.png"></a></div>
			<div><a id="astory" class="<?php echo Request::is('story')?'active':''; ?>" data-slide-index="1" href="<?php echo URL::to('story'); ?>">story</a></div>
			<div><a id="ayarns" class="<?php echo Request::is('our-yarns')?'active':''; ?>" data-slide-index="2" href="<?php echo URL::to('our-yarns'); ?>">our yarns</a></div>
			<div><a id="apatterns" class="<?php echo Request::is('our-patterns')?'active':''; ?>" data-slide-index="3" href="<?php echo URL::to('our-patterns'); ?>">our patterns</a></div>
			<div><a id="astore" target="_blank"  href="http://www.berroco.com/locator/amano.php">store finder</a></div>
			<div id="contacontact"><a data-slide-index="4" href="<?php echo URL::to('contact-us'); ?>">contact us</a><span id="contsocial"><a  target="_blank" id="instagram" href="https://instagram.com/amanoyarns/ " class="social"><span class="icon icon-instagram"></span></a><a target="_blank" href="http://www.ravelry.com/yarns/brands/amano-yarns" class="social"><img width="16px" src="<?php echo $urlimg; ?>icor.png"></a><a target="_blank" href="https://www.facebook.com/AmanoYarns" class="social"><span class=" icon icon-facebook2"></span></a></span></div>

		</div>



		<?php

		echo '<script src="'.$urljs.'jquery-1.11.3.min.js" type="text/javascript"></script>
			<script src="'.$urljs.'bootstrap.min.js" type="text/javascript"></script>';

			//<script src="'.$urljs.'jquery.bxslider.min.js" type="text/javascript"></script>
			//<script src="'.$urljs.'home.js" type="text/javascript"></script>
		if (isset($jscript)){
			foreach ($jscript as $value) {
				echo '<script src="'.$urljs.$value.'.js" type="text/javascript"></script>';
			}
		}
		?>

	</body>
</html>
