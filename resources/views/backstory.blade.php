@extends('web')
@section('content')
<?php
$iurl=asset('/');
$urljs=$iurl.'js/';
$urlimg=$iurl.'img/';
?>
	<div id="contvideo" class="videostory">
		<video id="video" width="100%"  autoplay loop>
		  <source src="<?php echo $iurl; ?>video/video.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
	</div>
	<div id="contslider">
		  		<div>
		  			<div id="logo2"><img src="<?php echo $urlimg; ?>logo2.png"></div>
		  			<div id="texto">
		  				<div class="1">FOR  THOUSANDS OF YEARS THE PEOPLE OF THE ANDES HAVE MADE OFFERINGS TO THE GODS</div>
						<div class="2">THAT THEY BELIEVE DWELL IN THE MOUNTAINS, THE EARTH THE RIVERS AND THE SKY.</div>
						<div class="3">WITH AMANO WE WANT TO BRING BACK THOSE MAGICAL BELIEFS TO OUR TIME AND THROUGH OUR HANDS,</div>
						<div class="4">WITH CREATIV TY, FEE THE NATURAL RESOURCES OF THE PERUVIAN ANDES OURSELVES.</div>
						<div class="5">AMANO IS LUXURY IN ITS PUREST EXPRESSION AND REPRESENTS GENEROSITY,</div>
						<div class="6">A GIFT FROM GOODS, A TASTE OF THE ANDES.</div>
		  			</div>
		  		</div>
	</div>
	@endsection