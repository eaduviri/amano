@extends('web')
@section('content')
<?php
$iurl=asset('/');
$urljs=$iurl.'js/';
$urlimg=$iurl.'img/';
?>

	<div id="contslider">
		<div id="loading" class="loading"><div><img src="<?php echo $urlimg; ?>loading.gif"></div></div>
		<div id="contbigimg"><div id="velo"></div><div id="imgbig"><img id="imggbig" src=""></div></div>
			<div id="bxslider">


		  		<!--/////////////////patterns////////////////-->
		  		<!--/////////////////patterns////////////////-->
		  		<!--/////////////////patterns////////////////-->
		  		<div id="contpatterns">
		  		<div id="patterns">
		  				<div id="patternsslider">
		  					<!--////////////////////////APU////////////////////////-->
		  					<div>
		  						<div id="indexpatterns0" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider0" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">laura cowl</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">

						  								<div class="pattern_buy">
						  									<div>
						  									Infinite soft yarn.<br>
						  									Lightweight. Natural<br>
						  									raw white
						  									</div>
						  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Apu" target="_blank" class="box_buy">
						  										buy this <br> pattern
						  									</a>
						  								</div>
						  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun0" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun0" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">maria cowl</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Apu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun1" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun1" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>

					  		</div>
					  		<!--////////////////////////END APU////////////////////////-->

					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns1" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a  data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider1" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">rosina mittens</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Mayu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun01" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun01" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">elsa vest</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Mayu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun11" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun11" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">paulina cowl</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Mayu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun21" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun21" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">juanita cowl</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Mayu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun31" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun31" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">antonia cardigan</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Mayu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun41" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun41" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////END MAYU////////////////////////-->
					  		<!--////////////////////////END MAYU////////////////////////-->

					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns2" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a  data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider2" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">gloria hat</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Puyu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun02" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun02" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">alejandrina shawl</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Puyu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun12" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun12" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">jacinta capelet</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Puyu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
																3 Hanks AMANO PUYU (50 grs), #3001 Beige <br>
																24” Length circular knitting needle, size 13 (9.00 mm) OR SIZE TO OBTAIN GAUGE <br>
																Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun22" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun22" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">marcelina pullover</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Puyu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun32" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun32" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">josefina pullover</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Puyu" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun42" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun42" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns3" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider3" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">frida scarf</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Puna" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun03" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun03" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">dorita pullover</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Puna" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun13" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun13" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">delia cowl</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Puna" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun23" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun23" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>


				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns4" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider4" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">isabel shawl</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Warmi" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun04" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun04" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->
				  						<div>
				  							<h1 class="text-center">hilda hat</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Warmi" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun14" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun14" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">anita wrap vest</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Warmi" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun24" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun24" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<div>
				  							<h1 class="text-center">dominga top</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Warmi" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun34" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun34" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<!--////////////////////////MAYU////////////////////////-->
					  		<div>
					  			<div id="indexpatterns5" class="indexpatterns">
					  				<div><a  data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a  data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a  data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a  data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider5" class="sliderpatterns">
				  						<!--////////////////////////apu1////////////////////////-->
				  						<div>
					  						<h1 class="text-center">benita scarft</h1>
					  						<div class="patternlevel">
					  							<b>skill level</b><img width="12" src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto0.png"><img width="12"  src="<?php echo $urlimg; ?>punto1.png"> Intermediate <span><b>one size</b></span>
					  						</div>
					  						<div class="contalbun">
					  							<div class="col-sm-6 text-right">
					  								<div class="pattern_buy">
					  									<div>
					  									Infinite soft yarn.<br>
					  									Lightweight. Natural<br>
					  									raw white
					  									</div>
					  									<a href="http://www.berroco.com/locator/amano.php?brand=Amano&yarn=Ayni" target="_blank" class="box_buy">
					  										buy this <br> pattern
					  									</a>
					  								</div>
					  								<div class="contmaterial">
						  									<div>
							  									<b>materials</b><br>
								  								2 Balls AMANO APU (25grs).<br>
								  								in color # 1000 Pure White<br>
								  								16'' Circular Knitting needle size 4(3.50 mm)<br>
								  								Stitch marker
						  									</div>
						  								</div>
					  								<div id="indexalbun05" class="indexalbun">
					  									<!--
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				<a data-slide-index="0"><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></a>
										  				-->

						  							</div>
					  							</div>
					  							<div class="col-sm-6">
					  							<div id="albun05" class="albun">
				  									<!--
					  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
							  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
							  						-->
					  							</div>
					  							</div>

					  						</div>
				  						</div>
				  						<!--////////////////////////end apu1////////////////////////-->

				  					</div>
					  			</div>
					  		</div>
					  		<!--////////////////////////MAYU////////////////////////-->

		  				</div>
		  			</div>

		  			<div class="indexgalery">
		  				<div id="pattersindex">
		  					<!--
			  				<a id="aapu2" data-slide-index="0"></a>
				  			<a id="amayu2" data-slide-index="1"></a>
				  			<a id="apuyu2"  data-slide-index="2"></a>
				  			<a id="apuna2"  data-slide-index="3"></a>
				  			<a id="awarmi2"  data-slide-index="4"></a>
				  			<a id="aayni2"  data-slide-index="5"></a>
				  			-->
		  				</div>
		  			</div>


		  		</div>
		  		<!--/////////////////end patterns////////////////-->

			</div>
		</div>
		<div>

		</div>
		<a href="" id="acomodin"></a>
@endsection