<?php
$iurl=asset('/');
$urljs=$iurl.'js/';
$urlimg=$iurl.'img/';
?>
<html>
	<head>
		<title>Amano</title>

		<?php
		 echo
		 '<link rel="stylesheet" type="text/css" href="'.$iurl.'/css/style.css">
		 <link rel="stylesheet" type="text/css" href="'.$iurl.'/css/app.css">
		 '
		 ; ?>
	</head>
	<body>
		<div id="contslider">
			<div id="bxslider">
		  		<div><img src="<?php echo $urlimg; ?>img1.jpg" /></div>
		  		<div><img src="<?php echo $urlimg; ?>img2.jpg" /></div>
		  		<!--///////////////// yarnss////////////////-->
		  		<div id="contyarns">
		  			<div id="yarns">
		  				<div id="yarnsslider">
					  		<div>
					  			<div class="contcolorslider">
				  					<div class="colorslider">
				  						<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
				  					</div>
					  			</div>
					  			<div class="indexcolors">
					  				<a data-slide-index="0" style="background-color:#F2DECB;"></a>
					  				<a data-slide-index="1" style="background-color:#AA7A49;"></a>
					  				<a data-slide-index="2" style="background-color:#66302B;"></a>
					  				<a data-slide-index="3" style="background-color:#872E1F;"></a>
					  				<a data-slide-index="4" style="background-color:#D86F34;"></a>
					  				<a data-slide-index="5" style="background-color:#D64F60;"></a>
					  				<a data-slide-index="6" style="background-color:#7C3246;"></a>
					  				<a data-slide-index="7" style="background-color:#18728A;"></a>
					  				<a data-slide-index="8" style="background-color:#3C5D67;"></a>
					  				<a data-slide-index="9" style="background-color:#856D65;"></a>
					  				<a data-slide-index="10" style="background-color:#E7DFD8;"></a>
					  			</div>
					  		</div>
		  					<div>
		  						<!--
		  						<div class="indexpatterns">
					  				<div><a data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div class="sliderpatterns">
				  						<div>
				  						<h1 class="text-center">laura cowl</h1>
				  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
				  						<div class="contalbun">
				  							<div class="col-sm-6 text-right">
				  								<div class="pattern_buy">
				  									<div>
				  									Infinite soft yarn.<br>
				  									Lightweight. Natural<br>
				  									raw white
				  									</div>
				  									<div class="box_buy">
				  									</div>
				  								</div>
				  								<div >
				  									<b>materials</b><br>
					  								2 Balls AMANO APU (25grs).<br>
					  								in color # 1000 Pure White<br>
					  								16'' Circular Knitting needle size 4(3.50 mm)<br>
					  								Stitch marker
				  								</div>
				  								<div class="indexalbun">
					  								<a data-slide-index="0" style="background-color:#F2DECB;"></a>
									  				<a data-slide-index="1" style="background-color:#AA7A49;"></a>
									  				<a data-slide-index="2" style="background-color:#66302B;"></a>
					  							</div>
				  							</div>
				  							<div class="col-sm-6">
				  							<div class="albun">
				  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
						  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
						  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
				  							</div>
				  							</div>

				  						</div>
				  						</div>
				  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
				  					</div>
					  			</div>
					  			-->
					  		</div>
					  		<div><img src="<?php echo $urlimg; ?>img1.jpg" /></div>
					  		<div><img src="<?php echo $urlimg; ?>img2.jpg" /></div>
					  		<div><img src="<?php echo $urlimg; ?>img1.jpg" /></div>
					  		<div><img src="<?php echo $urlimg; ?>img2.jpg" /></div>
		  				</div>
		  			</div>
		  			<div id="yarnsindex">
			  			<div class="indexgalery">
			  				<a id="aapu" data-slide-index="0"></a>
				  			<a id="amayu" data-slide-index="1"></a>
				  			<a id="apuyu"  data-slide-index="2"></a>
				  			<a id="apuna"  data-slide-index="3"></a>
				  			<a id="awarmi"  data-slide-index="4"></a>
				  			<a id="aayni"  data-slide-index="5"></a>
			  			</div>
		  			</div>
		  		</div>
		  		<!--/////////////////end yarnss////////////////-->
		  		<!--/////////////////patterns////////////////-->
		  		<div>
		  		<div id="patterns">
		  				<div id="patternsslider">
					  		<div>
					  		</div>
		  					<div>
		  						<div id="indexpatterns0" class="indexpatterns">
					  				<div><a data-slide-index="0" style="background-color:#F2DECB;"></a></div>
					  				<div><a data-slide-index="1" style="background-color:#AA7A49;"></a></div>
					  				<div><a data-slide-index="2" style="background-color:#66302B;"></a></div>
					  				<div><a data-slide-index="3" style="background-color:#872E1F;"></a></div>
					  				<div><a data-slide-index="4" style="background-color:#D86F34;"></a></div>
					  			</div>
					  			<div class="patters">
				  					<div id="patternsslider2" class="sliderpatterns">
				  						<div>
				  						<h1 class="text-center">laura cowl</h1>
				  						<div class="patternlevel"><b>skill level</b> Intermediate <b>one size</b></div>
				  						<div class="contalbun">
				  							<div class="col-sm-6 text-right">
				  								<div class="pattern_buy">
				  									<div>
				  									Infinite soft yarn.<br>
				  									Lightweight. Natural<br>
				  									raw white
				  									</div>
				  									<div class="box_buy">
				  									</div>
				  								</div>
				  								<div >
				  									<b>materials</b><br>
					  								2 Balls AMANO APU (25grs).<br>
					  								in color # 1000 Pure White<br>
					  								16'' Circular Knitting needle size 4(3.50 mm)<br>
					  								Stitch marker
				  								</div>
				  								<div class="indexalbun">
					  								<a data-slide-index="0" style="background-color:#F2DECB;"></a>
									  				<a data-slide-index="1" style="background-color:#AA7A49;"></a>
									  				<a data-slide-index="2" style="background-color:#66302B;"></a>
					  							</div>
				  							</div>
				  							<div class="col-sm-6">
				  							<div class="albun">
				  								<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
						  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
						  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
				  							</div>
				  							</div>

				  						</div>
				  						</div>
				  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanagris.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaroja.jpg" /></div>
				  						<div><img src="<?php echo $urlimg; ?>lanaazul.jpg" /></div>
				  					</div>
					  			</div>
					  		</div>
					  		<div><img src="<?php echo $urlimg; ?>img1.jpg" /></div>
					  		<div><img src="<?php echo $urlimg; ?>img2.jpg" /></div>
					  		<div><img src="<?php echo $urlimg; ?>img1.jpg" /></div>
					  		<div><img src="<?php echo $urlimg; ?>img2.jpg" /></div>
		  				</div>
		  			</div>

		  			<div id="pattersindex">
		  				<div class="indexgalery">
			  				<a id="aapu2" data-slide-index="0"></a>
				  			<a id="amayu2" data-slide-index="1"></a>
				  			<a id="apuyu2"  data-slide-index="2"></a>
				  			<a id="apuna2"  data-slide-index="3"></a>
				  			<a id="awarmi2"  data-slide-index="4"></a>
				  			<a id="aayni2"  data-slide-index="5"></a>
		  				</div>
		  			</div>


		  		</div>
		  		<!--/////////////////end patterns////////////////-->
		  		<div><img src="<?php echo $urlimg; ?>img1.jpg" /></div>
		  		<div><img src="<?php echo $urlimg; ?>img2.jpg" /></div>
			</div>
		</div>
		<div>

		</div>

		<div id="bx-pager">
			<div><a data-slide-index="0" href="">logo</a></div>
			<div><a data-slide-index="1" href="">story</a></div>
			<div><a data-slide-index="2" href="">our yarns</a></div>
			<div><a id="apatterns" data-slide-index="3" href="">our patterns</a></div>
			<div><a data-slide-index="4" href="">store finder</a></div>
			<div><a data-slide-index="5" href="">contact us</a><a class="social"><span class="icon icon-instagram"></span></a><a class="social">r</a><a class="social"><span class=" icon icon-facebook2"></span></a></div>
			<!--<span class="icon icon-twitter2"></span>-->
		</div>


		<?php
		echo '<script src="'.$urljs.'jquery-1.11.3.min.js" type="text/javascript"></script>
			<script src="'.$urljs.'jquery.bxslider.min.js" type="text/javascript"></script>
			<script src="'.$urljs.'home.js" type="text/javascript"></script>';
		?>
	</body>
</html>
