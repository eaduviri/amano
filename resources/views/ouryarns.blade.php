@extends('web')

@section('title') Our Yarns @endsection

@section('content')
	<?php
	$iurl = asset('/');
	$urljs = $iurl . 'js/';
	$urlimg = $iurl . 'img/';
	?>
	<div id="contslider">
		<div id="bxslider">
			<div id="contyarns">

				<div id="yarns">
					<div id="yarnsslider">
						<!--///////////////// APU YARNS////////////////-->
						<!--///////////////// APU YARNS////////////////-->
						<div>
							<!--/////////////////APU COLORS////////////////-->
							<div id="ttllogo1" class="ttllogo"></div>
							<div>
								<div class="contcolorslider">
									<div class="colorslider" id="colorslider0">

									</div>
								</div>

								<div id="indexcolors0" class="indexcolors"></div>
							</div>



							<h2>100% Imperial Alpaca</h2>


							<div class="subttlcolors">weight 25gr | approx. 109yd / 100m | needle size 4 US</div>
							<div class="conttxtcolors">
								Apu in quechua (the native language of the Incas) means mountain god. The Incas believed that the highest mountains of the Andes were extensions of the earth goddess (Pachamama) and places separated for the most sacred rituals. For the Inca, the finest alpaca textile were comparable in value to gold and the finest jewelry. For Amano, Apu represents a gift from the gods. The finest alpaca in the world. Imperial Alpaca (17 mic.) sorted and dehaired by hand, presented in its most natural state, raw white.
							</div>
							<!--/////////////////END APU COLORS////////////////-->
						</div>
						<!--/////////////////END APU YARNS////////////////-->
						<!--/////////////////END APU YARNS////////////////-->

						<!--///////////////// MAYU YARNS////////////////-->
						<!--///////////////// MAYU YARNS////////////////-->
						<div>
							<!--/////////////////APU COLORS////////////////-->
							<div id="ttllogo2" class="ttllogo"></div>
							<div>
								<div class="contcolorslider">
									<div class="colorslider" id="colorslider1">
									</div>
								</div>

								<div id="indexcolors1" class="indexcolors">
								</div>
							</div>

							<h2>60% Royal Alpaca · 20% Cashmere · 20% Silk</h2>

							<div class="subttlcolors">weight 50gr| approx. 109yd / 100m | needle size 5 US</div>
							<div class="conttxtcolors">
								Mayu in Quechua (the native language of the Incas) means river. This unique blend of Royal Alpaca, Cashmere and Mulberry Silk has an incomparable softness and fluid appearance. All colors are dip dyed imitating a dyeing technique used by the Incas. The inspiration for colors comes from the soft landscapes of the Peruvian Andes mountains.
							</div>
							<!--/////////////////END APU COLORS////////////////-->
						</div>
						<!--/////////////////END MAYU YARNS////////////////-->
						<!--/////////////////END MAYU YARNS////////////////-->

						<!--///////////////// PUYU YARNS////////////////-->
						<!--///////////////// PUYU YARNS////////////////-->
						<div>
							<!--/////////////////PUYU COLORS////////////////-->
							<div id="ttllogo3" class="ttllogo"></div>
							<div>
								<div class="contcolorslider">
									<div class="colorslider" id="colorslider2">
									</div>
								</div>

								<div id="indexcolors2" class="indexcolors">
								</div>
							</div>

							<h2>70% Baby Alpaca 30% Silk</h2>

							<div class="subttlcolors">weight 50gr | approx. 82yd / 75m | needle size 13 US</div>
							<div class="conttxtcolors">
								Puyu in quechua (the native language of the Incas) means cloud. This yarn is Baby Alpaca introduced by high air pressure into a cage of Mulberry Silk. This technique allows us to present the lightest chunky yarn made with Alpaca. The muse for the color card is the Alpaca and its unique range of natural colors found in herds in the Peruvian Andes.
							</div>
							<!--/////////////////END PUYU COLORS////////////////-->
						</div>
						<!--/////////////////END PUYU YARNS////////////////-->
						<!--/////////////////END PUYU YARNS////////////////-->


						<!--///////////////// PUNA YARNS////////////////-->
						<!--///////////////// PUNA YARNS////////////////-->
						<div>
							<!--/////////////////PUNA COLORS////////////////-->
							<div id="ttllogo4" class="ttllogo"></div>
							<div>
								<div class="contcolorslider">
									<div class="colorslider" id="colorslider3">
									</div>
								</div>

								<div id="indexcolors3" class="indexcolors">
								</div>
							</div>

							<h2>100% Baby Alpaca</h2>

							<div class="subttlcolors">weight 100gr | approx. 273yd / 250m | needle size 6 US</div>
							<div class="conttxtcolors">
								Puna in quechua (the native language of the Incas) means Andes mountain. Puna is the essence of the Andes as the Alpaca is the essence of Amano. This yarn is 100% Baby Alpaca presented only in melange colors inspired by unique and high contrast landscapes found in the Andes.
							</div>
							<!--/////////////////END PUNA COLORS////////////////-->
						</div>
						<!--///////////////// END PUNA YARNS////////////////-->
						<!--///////////////// END PUNA YARNS////////////////-->

						<!--///////////////// WARMI YARNS////////////////-->
						<!--///////////////// WARMI YARNS////////////////-->
						<div>
							<!--/////////////////WARMI COLORS////////////////-->
							<div id="ttllogo5" class="ttllogo"></div>
							<div>
								<div class="contcolorslider">
									<div class="colorslider" id="colorslider4">
									</div>
								</div>

								<div id="indexcolors4" class="indexcolors">
								</div>
							</div>

							<h2>70% Baby Alpaca 30% Merino Wool</h2>

							<div class="subttlcolors">weight 100gr | approx. 164yd / 150m | needle size 8 US</div>
							<div class="conttxtcolors">
								Warmi in quechua (the native language of the Incas) means woman or union. For Amano, Warmi is the union of two unique fibers: Baby Alpaca and Merino Wool. The inspiration for the color card comes from the inimitable palette of Peruvian fruits and flowers. Warmi is presented exclusively in melange colors.
							</div>

							<!--/////////////////END WARMI COLORS////////////////-->
						</div>
						<!--///////////////// END WARMI YARNS////////////////-->
						<!--///////////////// END WARMI YARNS////////////////-->

						<!--///////////////// AYMI YARNS////////////////-->
						<!--///////////////// AYMI YARNS////////////////-->
						<div>
							<!--/////////////////AYMI COLORS////////////////-->
							<div id="ttllogo6" class="ttllogo"></div>
							<div>
								<div class="contcolorslider">
									<div class="colorslider" id="colorslider5">
									</div>
								</div>

								<div id="indexcolors5" class="indexcolors">
								</div>
							</div>

							<h2>80% Baby Alpaca 20% Silk</h2>

							<div class="subttlcolors">weight 50gr | approx. 218yd / 200m | needle size 4 US</div>
							<div class="conttxtcolors">
								Ayni in Quechua (the native language of the Incas) means exchange system. This system of barter trade for products and services still exist in some Andean communities. For Amano, Ayni represents a unique blend of raw materials: Baby Alpaca and Mulberry Silk. The color card is inspired on items still traded in Andean markets by the Ayni exchange system.
							</div>
							<!--/////////////////END AYMI COLORS////////////////-->
						</div>
						<!--///////////////// END AYMI YARNS////////////////-->
						<!--///////////////// END AYMI YARNS////////////////-->
					</div>
				</div>


				<div class="indexgalery">
					<div id="yarnsindex">
						<!--<a id="aapu" data-slide-index="0"></a>
						<a id="amayu" data-slide-index="1"></a>
						<a id="apuyu"  data-slide-index="2"></a>
						<a id="apuna"  data-slide-index="3"></a>
						<a id="awarmi"  data-slide-index="4"></a>
						<a id="aayni"  data-slide-index="5"></a>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<<a href="" id="acomodin"></a>
@endsection