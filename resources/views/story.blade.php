@extends('web')

@section('title') Story @endsection

@section('content')
<?php
$iurl=asset('/');
$urljs=$iurl.'js/';
$urlimg=$iurl.'img/';
?>
<div id="loading" class="loading" ><div><img src="<?php echo $urlimg; ?>loading.gif"></div></div>
	<div id="contvideo" class="videostory">
		<div class="cristal"></div>
		<iframe id="video" src="https://player.vimeo.com/video/140729562?autoplay=1&title=0&byline=0&portrait=0&loop=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>
	<div id="contslider">
		  		<div>
		  			<div id="logo2"><img src="<?php echo $urlimg; ?>logo2.png"></div>
		  			<div id="textoimg">
		  				<img src="<?php echo $urlimg; ?>letras.png">
		  			</div>
		  		</div>
	</div>
	@endsection