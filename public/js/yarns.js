var hh = window.innerHeight;
var ww = window.innerWidth;
var token = {patterns: 0, yarns: 0};




var imgcolors = {
	apu: ['Apu.jpg'],
	mayu: ['Mayu1.jpg', 'Mayu2.jpg', 'Mayu3.jpg', 'Mayu4.jpg', 'Mayu5.jpg', 'Mayu6.jpg', 'Mayu7.jpg', 'Mayu8.jpg', 'Mayu10.jpg', 'Mayu9.jpg', 'Mayu11.jpg', 'Mayu12.jpg'],
	puyu: ['Puyu1.jpg', 'Puyu2.jpg', 'Puyu3.jpg', 'Puyu4.jpg', 'Puyu5.jpg', 'Puyu6.jpg', 'Puyu7.jpg', 'Puyu8.jpg', 'Puyu9.jpg'],
	puna: ['Puna1.jpg', 'Puna2.jpg', 'Puna3.jpg', 'Puna4.jpg', 'Puna5.jpg', 'Puna6.jpg', 'Puna7.jpg', 'Puna8.jpg', 'Puna9.jpg', 'Puna10.jpg', 'Puna11.jpg'],
	warmi: ['Warmi1.jpg', 'Warmi2.jpg', 'Warmi3.jpg', 'Warmi4.jpg', 'Warmi5.jpg', 'Warmi6.jpg', 'Warmi7.jpg', 'Warmi8.jpg', 'Warmi9.jpg', 'Warmi10.jpg', 'Warmi11.jpg'],
	ayni: ['Ayni1.jpg', 'Ayni2.jpg', 'Ayni3.jpg', 'Ayni4.jpg', 'Ayni5.jpg', 'Ayni6.jpg', 'Ayni7.jpg', 'Ayni8.jpg', 'Ayni9.jpg', 'Ayni10.jpg', 'Ayni11.jpg'],
};

var namecolors=[
	['Pure White 1000'],
	['Frost White 2000','Green Grass 2001','Rockwood 2002','Earth Brown 2003','Yellow Grass 2004','Exotic Bloom 2005','Wild Cherry 2006','Sierra Night 2007','Sky Blue 2008','Mistery Lake 2009','Stone Grey 2010','Celestial 2011'],
	['Cloud 3000',
'Beige 3001',
'Almond 3002',
'Caramel 3003',
'Chestnut 3004',
'Charcoal 3005',
'Dim Grey 3006',
'Silver 3007',
'Fog 3008'],
	['Maras White 4000',
'Andean Sunrise 4001',
'Machu Picchu 4002',
'Sacred Valley 4003',
'Misti Grey  4004',
'Alpamayo 4005',
'Inca Train 4006',
'Titicaca 4007',
'Santa Catalina 4008',
'Cusco Roofs 4009',
'Inca Ruins 4010'],
	['White Rose 6000',
'Wheat 6001',
'Toasted Coffee 6002',
'Quinua 6003',
'Pepper Red 6004',
'Cantu 6005',
'Beetroot 6006',
'Olive Green 6007',
'Orchid 6008',
'Blue Berry 6009',
'Chia 6010'],
	['Salt White 5000',
'Nutmeg 5001',
'Paprika 5002',
'Saffron 5003',
'Purple Corn 5004',
'Raisins 5005',
'Mint 5006',
'Poppy Blue 5007',
'Black Pepper 5008',
'Sunflower Seed 5009',
'Quartz Grey 5010']
];

var colorlist=[
[],
[/*----------------------mayu*/
'#DAD4CB',
'#433C20',
'#2F1C17',
'#431810',
'#AA6529',
'#BC4D53',
'#9E5C78',
'#72465E',
'#38507C',
'#314048',
'#8B8279',
'#99A7AD'
],
[/*----------------------puyu*/
'#d4ccc6',
'#a4968f',
'#8d634a',
'#954c28',
'#806f65',
'#2b2b2b',
'#49474c',
'#a49785',
'#c0baad'
],
[/*----------------------puna*/
'#e4e0d9',
'#cdaf7b',
'#a3a858',
'#805f33',
'#aba292',
'#7d604c',
'#393a45',
'#4c6e99',
'#571d1e',
'#db5549',
'#deb393'
],
[/*----------------------warmi*/
'#e8e0d3',
'#e8cead',
'#836444',
'#a56329',
'#af5835',
'#891136',
'#4a273a',
'#425630',
'#025e94',
'#2f4c5a',
'#d4d0c9'
],
[/*----------------------ayni*/
'#eeece4',
'#7d562c',
'#a21d15',
'#9c2859',
'#503044',
'#4c2a2a',
'#045361',
'#293046',
'#1d1813',
'#858585',
'#8d8c83'
]
];

var colorbord=[
[],
[/*----------------------mayu*/
'#fcf7fe',
'#766f53',
'#5F4f4a',
'#764b43',
'#dd985c',
'#ef7f86',
'#cf8fab',
'#a5798f',
'#64737b',
'#96AAD0',
'#beb5ac',
'#ccdadf'
],
[/*----------------------puyu*/
'#e9e4e1',
'#cdc4bf',
'#be9f8a',
'#c38c65',
'#b5a9a1',
'#696969',
'#89878c',
'#cdc4b8',
'#dddad2'
],
[/*----------------------puna*/
'#f0eeea',
'#e3d1ae',
'#c9cd92',
'#b1986d',
'#cfc9be',
'#af9887',
'#747580',
'#87a4c3',
'#915153',
'#eb8f84',
'#edd4bf'
],
[/*----------------------warmi*/
'#f3eee7',
'#f3e4d0',
'#b49d80',
'#cb9c62',
'#d29371',
'#b93872',
'#866076',
'#7e916b',
'#0798c0',
'#6a8894',
'#e8e5e1'
],
[/*----------------------ayni*/
'#f6f5f1',
'#b09166',
'#c95242',
'#c56193',
'#8b6b80',
'#886464',
'#0f8e9a',
'#626b82',
'#52493d',
'#b6b6b6',
'#bbbbb4'
]
];

$(document).ready(function(){

if (token.yarns == 0) {
	var divmadeja = '';

	$('#yarnsindex').html(	'<div><a href="#apu" id="aapu" data-slide-index="0" onclick="ga(\'send\', \'event\', \'OurYarns-Apu\', \'click\', \'Pure White 1000\');"></a> </div>'+
							'<div><a href="#mayu" id="amayu" data-slide-index="1" onclick="ga(\'send\', \'event\', \'OurYarns-Mayu\', \'click\', \'Frost White 2000\');"></a></div>'+
							'<div><a href="#apu" id="apuyu"  data-slide-index="2" onclick="ga(\'send\', \'event\', \'OurYarns-Puyu\', \'click\', \'Cloud 3000\');"></a></div>'+
							'<div><a href="#puna" id="apuna"  data-slide-index="3" onclick="ga(\'send\', \'event\', \'OurYarns-Puna\', \'click\', \'Maras White 4000\');"></a></div>'+
							'<div><a href="#warmi" id="awarmi"  data-slide-index="4" onclick="ga(\'send\', \'event\', \'OurYarns-Warmi\', \'click\', \'White Rose 6000\');"></a></div>'+
							'<div><a href="#ayni" id="aayni"  data-slide-index="5" onclick="ga(\'send\', \'event\', \'OurYarns-Ayni\', \'click\', \'Salt White 5000\');"></a></div>');


	var URLhash = window.location.hash;
	var enlaceindex=0;
	if (URLhash=='#mayu'){
		enlaceindex=1;
	}else if (URLhash=='#puyu'){
		enlaceindex=2;
	}else if (URLhash=='#puna'){
		enlaceindex=3;
	}else if (URLhash=='#warmi'){
		enlaceindex=4;
	}else if (URLhash=='#ayni'){
		enlaceindex=5;
	}
	$('#yarnsslider').bxSlider({
		mode: 'fade',
		pagerCustom: '#yarnsindex',
		controls: true,
		startSlide:enlaceindex,
		nextText:'',
		prevText:'',
		onSlideBefore:function($slideElement, oldIndex, newIndex){
				if (newIndex==1){
				mayu();
				}else if (newIndex==2){
					puyu();
				}else if (newIndex==3){
					puna();
				}else if (newIndex==4){
					warmi();
				}else if (newIndex==5){
				 	ayni();
				}else{
					apu();
				}
		},
		onSliderLoad: function () {
			if (enlaceindex==1){
				mayu();
			}else if (enlaceindex==2){
				puyu();
			}else if (enlaceindex==3){
				puna();
			}else if (enlaceindex==4){
				warmi();
			}else if (enlaceindex==5){
			 	ayni();
			}else{
				apu();
			}
		}
	});

	$('#aayni').click(function(){
		ayni();
	});
	$('#awarmi').click(function(){
		warmi();
	});
	$('#amayu').click(function(){
		mayu();
	});
	$('#apuyu').click(function(){
		puyu();
	});
	$('#apuna').click(function(){
		puna();
	});
	$('#aapu').click(function(){
		apu();
	});
	/*----------YARNS----------*/
	/*----------YARNS----------*/
	/*----------MAYU----------*/







	/*----------AYMI----------*/

	/*----------end yarns----------*/
	/*----------end yarns----------*/


	token.yarns = 1;
}


//$('#yarns').css('height',hh-(76+150));
//$('#contslider').css('height', hh - 76);
});
	function apu(){
		if (imgcolors.apu.length != 1) {
				var indicemadeja = '';
				for (var i = 0; i < imgcolors.apu.length; i++) {
					divmadeja += '<div></div>';
				}
				;
				$('#indexcolors0').html(indicemadeja);
				//$('#indexcolors0').css('width',);
			}
			else {
				divmadeja = '<div></div>';
			}
			$('#acomodin').attr('href','#apu');
				var comodin =document.getElementById("acomodin");
		 		comodin.click();

			$('#colorslider0').html(divmadeja);
			$('#colorslider0 div').eq(0).html('<img src="' + urlimg + 'madejas/' + imgcolors.apu[0] + '" /><div>'+namecolors[0][0]+'</div>');

			$('#colorslider0').bxSlider({
				mode: 'fade',
				pagerCustom: '#indexcolors0',
				controls: false,
				onSliderLoad: function () {
					$('#indexcolors0 div a').click(function () {
						/*//////////////////////PONER UN SHIWT PARA QUE NO VUELVA A AÑADIR SI ENTRA MAS DE 2 VECES////////////////////////*/
						var aindex = $(this);
						if (!aindex.hasClass('ok')) {
							var index = abindex.attr('data-slide-index');
							$('#colorslider0 div').eq(index).html('<img src="' + urlimg + 'madejas/' + imgcolors.apu[index] + '"/><div>'+namecolors[0][index]+'</div>');
							aindex.addClass('ok');
						}
					});
				}
			});
	}
	var amayu = 0;
	function mayu() {
		if (amayu == 0) {
			var divmadeja = '';

			if (imgcolors.mayu.length > 1) {
				var indicemadeja = '';
				for (var i = 0; i < imgcolors.mayu.length; i++) {
					indicemadeja +='<div><a href="#" data-slide-index="'+i+'" style="background-color:'+colorlist[1][i]+';border-color:'+colorbord[1][i]+' !important;"></a></div>';
					divmadeja += '<div></div>';
				}
				$('#indexcolors1').html(indicemadeja);
			} else {
				divmadeja = '<div></div>';
			}
			$('#colorslider1').html(divmadeja);
			$('#colorslider1 div').eq(0).html('<img src="' + urlimg + 'madejas/' + imgcolors.mayu[0] + '" /><div>'+namecolors[1][0]+'</div>');

			$('#acomodin').attr('href','#mayu');
			var comodin =document.getElementById("acomodin");
	 		comodin.click();


			$('#colorslider1').bxSlider({
				mode: 'fade',
				pagerCustom: '#indexcolors1',
				controls: false,
				onSliderLoad: function () {
					$('#indexcolors1 a').click(function () {
						//alert($(this).index());
						var abindex = $(this);
						if (!abindex.hasClass('ok')) {
							var index = abindex.attr('data-slide-index');
							$('#colorslider1 > div').eq(index).html('<img src="' + urlimg + 'madejas/' + imgcolors.mayu[index] + '"/><div>'+namecolors[1][index]+'</div>');
							abindex.addClass('ok');
						}
					});
				}
			});
			amayu = 1;
		}
	}
	var apuyu = 0;
	function puyu() {
		if (apuyu == 0) {
			var divmadeja = '';
			if (imgcolors.puyu.length != 1) {
				var indicemadeja = '';
				for (var i = 0; i < imgcolors.puyu.length; i++) {
					indicemadeja +='<div><a href="#" data-slide-index="'+i+'" style="background-color:'+colorlist[2][i]+';border-color:'+colorbord[2][i]+' !important;"></a></div>';
					divmadeja += '<div></div>';
				}

				$('#indexcolors2').html(indicemadeja);
			}
			else {
				divmadeja = '<div></div>';
			}
			$('#acomodin').attr('href','#puyu');
			var comodin =document.getElementById("acomodin");
	 		comodin.click();
			$('#colorslider2').html(divmadeja);
			$('#colorslider2 div').eq(0).html('<img src="' + urlimg + 'madejas/' + imgcolors.puyu[0] + '" /><div>'+namecolors[2][0]+'</div>');
			$('#colorslider2').bxSlider({
				mode: 'fade',
				pagerCustom: '#indexcolors2',
				controls: false,
				onSliderLoad: function () {
					$('#indexcolors2 a').click(function () {
						var abindex = $(this);
						if (!abindex.hasClass('ok')) {
							var index = abindex.attr('data-slide-index');
							$('#colorslider2 > div').eq(index).html('<img src="' + urlimg + 'madejas/' + imgcolors.puyu[index] + '"/><div>'+namecolors[2][index]+'</div>');
							abindex.addClass('ok');
						}
					});
				}
			});
			apuyu = 1;
		}
	}
	/*----------PUNA----------*/
	var apuna = 0;
	function puna() {
		if (apuna == 0) {
			var divmadeja = '';
			if (imgcolors.puna.length > 1) {
				var indicemadeja = '';
				for (var i = 0; i < imgcolors.puna.length; i++) {
					indicemadeja +='<div><a href="#" data-slide-index="'+i+'" style="background-color:'+colorlist[3][i]+';border-color:'+colorbord[3][i]+' !important;"></a></div>';

					divmadeja += '<div></div>';
				}

				$('#indexcolors3').html(indicemadeja);
			} else {
				divmadeja = '<div></div>';
			}
			$('#acomodin').attr('href','#puna');
			var comodin =document.getElementById("acomodin");
	 		comodin.click();
			$('#colorslider3').html(divmadeja);
			$('#colorslider3 div').eq(0).html('<img src="' + urlimg + 'madejas/' + imgcolors.puna[0] + '" /><div>'+namecolors[3][0]+'</div>');
			$('#colorslider3').bxSlider({
				mode: 'fade',
				pagerCustom: '#indexcolors3',
				controls: false,
				onSliderLoad: function () {
					$('#indexcolors3 a').click(function () {
						var abindex = $(this);
						if (!abindex.hasClass('ok')) {
							var index = abindex.attr('data-slide-index');
							$('#colorslider3 > div').eq(index).html('<img src="' + urlimg + 'madejas/' + imgcolors.puna[index] + '"/><div>'+namecolors[3][index]+'</div>');
							abindex.addClass('ok');
						}
					});
				}
			});
			apuna = 1;
		}
	}

	/*----------WARMI----------*/
	var awarmi = 0;
	function warmi(){
		if (awarmi == 0) {
			var divmadeja = '';
			if (imgcolors.warmi.length != 1) {
				var indicemadeja = '';
				for (var i = 0; i < imgcolors.warmi.length; i++) {
					indicemadeja +='<div><a href="#" data-slide-index="'+i+'" style="background-color:'+colorlist[4][i]+';border-color:'+colorbord[4][i]+' !important;"></a></div>';
					divmadeja += '<div></div>';
				}
				;
				$('#indexcolors4').html(indicemadeja);
			} else {
				divmadeja = '<div></div>';
			}
			$('#acomodin').attr('href','#warmi');
			var comodin =document.getElementById("acomodin");
	 		comodin.click();
			$('#colorslider4').html(divmadeja);
			$('#colorslider4 div').eq(0).html('<img src="' + urlimg + 'madejas/' + imgcolors.warmi[0] + '" /><div>'+namecolors[4][0]+'</div>');
			$('#colorslider4').bxSlider({
				mode: 'fade',
				pagerCustom: '#indexcolors4',
				controls: false,
				onSliderLoad: function () {

					$('#indexcolors4 a').click(function () {
						var abindex = $(this);
						if (!abindex.hasClass('ok')) {
							abindex.addClass('ok');
							var index = abindex.attr('data-slide-index');
							$('#colorslider4 > div').eq(index).html('<img src="' + urlimg + 'madejas/' + imgcolors.warmi[index] + '"/><div>'+namecolors[4][index]+'</div>');
						}
					});
				}
			});
			awarmi = 1;
		}
	}

	var aayni = 0;
	function ayni(){

		if (aayni == 0) {
			var divmadeja = '';
			if (imgcolors.ayni.length != 1) {
				var indicemadeja = '';
				for (var i = 0; i < imgcolors.ayni.length; i++) {
					indicemadeja +='<div><a href="#" data-slide-index="'+i+'" style="background-color:'+colorlist[5][i]+';border-color:'+colorbord[5][i]+' !important;"></a></div>';
					divmadeja += '<div></div>';
				}
				$('#indexcolors5').html(indicemadeja);
			} else {
				divmadeja = '<div></div>';
			}
			$('#acomodin').attr('href','#ayni');
			var comodin =document.getElementById("acomodin");
	 		comodin.click();
			$('#colorslider5').html(divmadeja);
			$('#colorslider5 div').eq(0).prepend('<img src="' + urlimg + 'madejas/' + imgcolors.ayni[0] + '" /><div>'+namecolors[5][0]+'</div>');
			$('#colorslider5').bxSlider({
				mode: 'fade',
				pagerCustom: '#indexcolors5',
				controls: false,
				onSliderLoad: function () {
					$('#indexcolors5 a').click(function () {
						var abindex = $(this);
						if (!abindex.hasClass('ok')) {

							abindex.addClass('ok');
							var index = abindex.attr('data-slide-index');
							$('#colorslider5 > div').eq(index).html('<img src="' + urlimg + 'madejas/' + imgcolors.ayni[index] + '"/><div>'+namecolors[5][index]+'</div>');
						}
					});
				}
			});
			aayni = 1;
		}
	};



